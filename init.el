;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

;; Toglie l'assegnazione dell'azione alla combinazione di tasti CTRL-z
;; In Emacs eseguito in terminale xterm, CTRL-Z chiude bruscamente
;; Emsce (esegue un kill del processo)
(global-unset-key (kbd "C-z"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (wheatgrass)))
 '(custom-safe-themes
   (quote
    ("732b807b0543855541743429c9979ebfb363e27ec91e82f463c91e68c772f6e3" "a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" default)))
 '(package-selected-packages
   (quote
    (material-theme web-mode python-mode magit jedi flymake-python-pyflakes flycheck find-file-in-repository elpy beacon autopair))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.


 ;; Controlla se emacs e' eseguito in terminal mode
 ;; e in quel caso (cioe' no display-graphic-p)
 ;; applica delle variazioni al tema per rendere alcuni
 ;; elementi leggibili (altrimenti in xterm alcune cose non
 ;; lo erano)
 ;; 20191002 - Qualche perplessit� sul funzionamento corretto
 ;; in caso di emacs -nw da cmd windows.......
 (unless (display-graphic-p)
   
;; Modifiche ai colori standard per consentire la gestione del programma
;; anche in terminal xterm (alcuni colori blue non erano leggibili)
 '(button ((t (:inherit link :foreground "cyan"))))
 '(font-lock-comment-face ((t (:background "yellow" :foreground "magenta"))))
 '(font-lock-function-name-face ((t (:foreground "magenta" :weight bold))))
 '(ido-virtual ((t (:inherit font-lock-builtin-face :background "white"))))
 '(link ((t (:foreground "yellow" :underline t))))
 '(minibuffer-prompt ((t (:background "yellow" :foreground "medium blue"))))
 '(py-builtins-face ((t (:inherit font-lock-builtin-face :foreground "yellow"))))
 '(py-decorators-face ((t (:foreground "cyan"))))
 '(py-exception-name-face ((t (:inherit font-lock-builtin-face :foreground "red"))))
 '(web-mode-block-control-face ((t (:inherit font-lock-preprocessor-face :background "yellow"))))
 '(web-mode-block-delimiter-face ((t (:inherit font-lock-preprocessor-face :background "yellow"))))
 )
 

 )

;; verifica la presenza del package package nella variabile features
;; di Emacs. Se non c'e' la carica (dovrebbe essere come l'import di Python)
(require 'package)
(package-initialize)

;; aggiunge un valore alla lista di sistema package-archives
;; In questo caso aggiunge alla lista i repo marmalade e melpa-stable
;; in modo che siano disponibili per l'installazione di pacchetti
;; (fatta con packages-list-packages
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")))         

;; dopo l'aggiunta dei repo, forza il refresh dell'elenco dei pacchetti
;; disponibili
(package-refresh-contents)

;; definisce una funzione che verifica la presenza
;; del package ed eventualmente lo installa
(defun install-if-needed (package)
  ;; macro corrispondente a
  ;; if ( package-installed-p package nil  (package-install package))
  (unless (package-installed-p package)
    (package-install package)))

;; make more packages available with the package installer
;; definisce la lista di pacchetti che devo verificare con install-if-needed di cui sopra
(setq to-install
      ;; '(python-mode magit yasnippet jedi auto-complete autopair find-file-in-repository flycheck))
      '(python-mode  ;; indispensabile per python
	elpy ;; pacchetto per gestire emacs come IDE per python
	jedi ;; pacchetto per avere auto completamento in Python
	flycheck ;; pacchetto per il controllo on-the-fly della sintassi (alternativo e sembra migliore rispetto a flymake installato con elpy
	web-mode ;; modalit� per la gestione delle pagine WEB (html, mako, php)
	tramp
	magit
	beacon ;; visualizza una barra colorata al cambio pagina. Molto utile con Emacs in xterm (a volte si perde il cursore)
	autopair ;; comodo per la gestione delle parantesi (aperta / chiusa)
	find-file-in-repository))

;; A mapping funciont applies a given function to each element of a list or other collection.
;; questo dovrebbe significare che per ogni elemento della lista to-install esegue install-if-needed
(mapc 'install-if-needed to-install)


(require 'magit)

;; associa un'azione un comando alla combinazione di tasti
;; CTRL+xg
(global-set-key "\C-xg" 'magit-status)

;(require 'auto-complete)
;(require 'autopair)
;(require 'yasnippet)
;(require 'flycheck)
;(global-flycheck-mode t)

(global-set-key [f7] 'find-file-in-repository)
(global-set-key [\C-f8] 'next-buffer)
(global-set-key [\C-f7] 'previous-buffer)

;; setta una environment variable. In questo caso aggiunge alla
;; variabile PATH il percorso di git, leggendo il valore attuale con getenv
(setenv "PATH" (concat "C:\\Users\\Dev\\PortableGit\\bin;" (getenv "PATH")))

; auto-complete mode extra settings
;(setq
; ac-auto-start 2
; ac-override-local-map nil
; ac-use-menu-map t
; ac-candidate-limit 20)

;; Python mode settings
(require 'python-mode)

;; aggiunge un valore alla lista di sistema auto-mode-alist
;; In questo caso aggiunge alla lista, l'associazione dei file
;; con estensione py e pyx alla major mode python-mode.
;; In questo modo all'apertura di un file py, Emacs
;; sa quale modalit� aprire
(add-to-list 'auto-mode-alist '("\\.py$" . python-mode))
(add-to-list 'auto-mode-alist '("\\.pyx" . python-mode))
;(setq py-electric-colon-active t)


;; add-hook aggancia le modalit� autopair yas-minor etc
;; alla modalit� python-mode.
;; Quindi quanto entro in python-mode, automaticamente sono attive
;; anche le modalit� qui sotto 
(add-hook 'python-mode-hook 'autopair-mode)
(add-hook 'python-mode-hook 'yas-minor-mode)
(add-hook 'python-mode-hook 'column-number-mode)
(add-hook 'python-mode-hook 'auto-complete-mode)

;; ;; Jedi settings
;(require 'jedi)
;; It's also required to run "pip install --user jedi" and "pip
;; install --user epc" to get the Python side of the library work
;; correctly.
;; With the same interpreter you're using.

;; if you need to change your python intepreter, if you want to change it
;; (setq jedi:server-command
;;       '("python2" "/home/andrea/.emacs.d/elpa/jedi-0.1.2/jediepcserver.py"))

(add-hook 'python-mode-hook
	  (lambda ()
	    (jedi:setup)
	    (jedi:ac-setup)
           (local-set-key "\C-cd" 'jedi:show-doc)
           (local-set-key (kbd "M-SPC") 'jedi:complete)
           (local-set-key (kbd "M-.") 'jedi:goto-definition)))



;; attiva la ido mode in tutti i moduli
;; la ido mode gestisce in maniera migliore
;; il find file, ed altre operazioni
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

;; abilita il elpy
(elpy-enable)
;; specifica cosa deve usare elpy per aprire la console di python
(setq python-shell-interpreter "ipython"
      python-shell-interpreter*args "-i --simple-prompt")
;; oltre a ipython puo' essere indicato jupyter notebook (non so come pero')
;; ma e' richeisto anche il pacchetto ein
;(setq python-shell-interpreter "jupyter"
;      python-shell-interpreter-args "console --simple-prompt"
;      python-shell-prompt-detect-failure-warning nil)
;(add-to-list 'python-shell-completion-native-disabled-interpreters
;             "jupyter")

;; con questo blocco, dico a elpy di utilizzare
;; flycheck per il controllo della sintassi al posto
;; di flymake
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))


;; web-mode
(require 'web-mode)

;; aggiunge un valore alla lista di sistema auto-mode-alist
;; In questo caso aggiunge alla lista, l'associazione dei file
;; con estensione phtml* alla major mode web-mode.
;; In questo modo all'apertura di un file phtml, Emacs
;; sa quale modalit� aprire
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mak\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))

;; A specific engine can be forced with web-mode-engines-alist.
(setq web-mode-engines-alist
      '(("php"    . "\\.phtml\\'")
        ("blade"  . "\\.blade\\.")
	("mako"   . "\\.mak\\."))
)

;; -------------------- extra nice things --------------------
;; use shift to move around windows
(windmove-default-keybindings 'shift)
(show-paren-mode t)
 ; Turn beep off


; Lasciato a nil, emette un suono
;(setq visible-bell nil)
; Impostato a zero, non emette nessun suono e non fa "il bell a schermo" (flasha lo schermo)
(setq visible-bell 0)

(beacon-mode 1)
(setq beacon-push-mark 35)
(setq beacon-color "#666600")

;; ------------------- tramp -------------------

;; C-x C-f /plink:<username>@<ipaddress>:/
(require 'tramp)
(set-default 'tramp-auto-save-directory "C:\\Users\\Alessio\\AppData\\Local\\Temp")
(set-default 'tramp-default-method "plink")




